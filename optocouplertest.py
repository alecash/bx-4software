#!/usr/bin/python3

import RPi.GPIO as GPIO
import time

GPIO.setmode(GPIO.BCM)
# Optocoupler Signal, Collector
GPIO.setup(3, GPIO.OUT)
# Emitter
GPIO.setup(5, GPIO.IN)

GPIO.output(3, HIGH)

GPIO.wait_for_edge(5, GPIO.RISING)
