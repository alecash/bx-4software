
#!/bin/bash
# Script: my-pi-temp.sh

cpu=$(</sys/class/thermal/thermal_zone0/temp)
echo "$(date) @ $(hostname)"
echo "----------------------"
echo "GPU => $(/opt/vc/bin/vcgencmd measure_temp)"
echo "CPU => $((cpu/1000))'C"

printf "$(date) @ $(hostname)" >> gpu.txt

printf "GPU => $(/opt/vc/bin/vcgencmd measure_temp)\n" >> gpu.txt

printf "$(date) @ $(hostname)" >> cpu.txt

cpu=$(</sys/class/thermal/thermal_zone0/temp)
printf "CPU => $((cpu/1000))'C\n" >> cpu.txt
