# Liam Spence

# Sends the servo pulses needed to initialise some ESCs
#
# Requires the pigpio daemon to be running
#
# sudo pigpiod

import time
import RPi.GPIO as GPIO   # Import the GPIO library.

import pigpio

ESC = 13

pi = pigpio.pi() # Connect to local Pi.

print ("This is the program used to command a motor during the thermal & power draw tests")

#pi.set_servo_pulsewidth(ESC, 1500) # 0 throttle

def one_direction():
    print("Waiting 5 seconds to clear the area, beginning the motor test...")
    time.sleep(5)

    for i in range(0, 10):
        print("spinning up...")
        pw = 1500;
        while pw < 2000:
            pw = pw+1;
            pi.set_servo_pulsewidth(ESC, pw) # 0 throttle
            time.sleep(i*0.01)
            print(pw)

        print("holding for 10 seconds, then spinning down to 0% throttle...")
        time.sleep(10)
              
        print("spinning down")

        while pw > 1500:
            pw = pw-1;
            pi.set_servo_pulsewidth(ESC, pw) # 0 throttle
            time.sleep(i*0.01)
            print(pw)

        print("holding for 2 seconds, then spinning up to 100% throttle...")
        time.sleep(2)

def two_directions():
    print("Waiting 5 seconds to clear the area, beginning the motor test...")
    time.sleep(5)
    pw = 1500;

    for i in range(0, 10):
        print("spinning up...")
        while pw < 2000:
            pw = pw+1;
            pi.set_servo_pulsewidth(ESC, pw) # 0 throttle
            time.sleep(i*0.01)
            print(pw)

        print("holding for 10 seconds, then spinning down to -100% throttle...")
        time.sleep(10)
        
        print("spinning down...")
 
        while pw > 1000:
            pw = pw-1;
            pi.set_servo_pulsewidth(ESC, pw) # 0 throttle
            time.sleep(i*0.01)
            print(pw)

        print("holding for 10 seconds, then spinning up to 100% throttle...")
        time.sleep(10)
    
    # ensure motor stops at 0% throttle
    while pw < 1500:
        pw = pw+1;
        pi.set_servo_pulsewidth(ESC, pw) # 0 throttle
        time.sleep(0.1)
        print(pw)
    
    print("should be 0% throttle, hold for 2 seconds, then shut off...")
    time.sleep(2)

def continuous():
    duration = input('How long, in seconds, do you want this to run for?')
    
    print("Waiting 5 seconds to clear the area, beginning the motor test...")
    time.sleep(5)
    pw = 1500;

    print("spinning up...")
    while pw < 2000:
        pw = pw+1;
        pi.set_servo_pulsewidth(ESC, pw) # 0 throttle
        print(pw)

    print("holding for specified duration...")
    time.sleep(duration)

    print("duration reached, spinning down...")

    # ensure motor stops at 0% throttle
    while pw > 1500:
        pw = pw-1;
        pi.set_servo_pulsewidth(ESC, pw) # 0 throttle
        time.sleep(0.1)
    
    print("should be 0% throttle, hold for 2 seconds, then shut off...")
    time.sleep(2)

# main loop of program
inp = input('Input "one", "two"')#, or "continuous"')
if inp == "one":
    one_direction()
elif inp == "two":
    two_directions()
#elif inp == "continuous":
#    continuous()
else :
    print("incorrect input") 

pi.set_servo_pulsewidth(ESC, 1500) # Stop servo pulses.

pi.stop() # Disconnect from local Raspberry Pi.
print("end")