# Simple PWM Test
# Liam Spence

# Sends the servo pulses needed to initialise some ESCs
#
# Requires the pigpio daemon to be running
#
# sudo pigpiod

import time
import RPi.GPIO as GPIO   # Import the GPIO library.

import pigpio

ESC = 13

pi = pigpio.pi() # Connect to local Pi.

# Calibrate ESC - *NOT NEEDED IF YOU HEAR LOW THEN HIGH BEEP WHEN CONNECTING TO RPI*
#print("Calibrating the ESC, should hear low then high beep")
#pi.set_servo_pulsewidth(SERVO, 1000) # Minimum throttle.
#time.sleep(1)
#pi.set_servo_pulsewidth(SERVO, 2000) # Maximum throttle.
#time.sleep(1)

#print("0 throttle for 3 seconds")
pi.set_servo_pulsewidth(ESC, 1500) # 0 throttle
#time.sleep(3)

print("spinning up, 5% every 20 seconds, up to 30%...")

i = 1500;
for inc in range(0,6):
    i = i+25; # increment by 5% of 500 micro sec
    pi.set_servo_pulsewidth(ESC, i) # increase throttle
    print(i) # print throttle    
    time.sleep(20) # hold for 10 sec
    
print("Holding 30% throttle for 30 seconds")
time.sleep(30)
print("throttle down to 0% immediately")
pi.set_servo_pulsewidth(ESC, 1500) # increase throttle

