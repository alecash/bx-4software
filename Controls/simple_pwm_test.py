# Simple PWM Test
# Liam Spence

# Sends the servo pulses needed to initialise some ESCs
#
# Requires the pigpio daemon to be running
#
# sudo pigpiod

import time
import RPi.GPIO as GPIO   # Import the GPIO library.

import pigpio

ESC = 13

pi = pigpio.pi() # Connect to local Pi.

# Calibrate ESC - *NOT NEEDED IF YOU HEAR LOW THEN HIGH BEEP WHEN CONNECTING TO RPI*
#print("Calibrating the ESC, should hear low then high beep")
#pi.set_servo_pulsewidth(SERVO, 1000) # Minimum throttle.
#time.sleep(1)
#pi.set_servo_pulsewidth(SERVO, 2000) # Maximum throttle.
#time.sleep(1)

print("0 throttle for 3 seconds, spin up to 20% throttle...")
pi.set_servo_pulsewidth(ESC, 1500) # 0 throttle
time.sleep(3)

print("spinning up...")

i = 1500;
while i < 1600:
    i = i+1;
    pi.set_servo_pulsewidth(ESC, i) # 0 throttle
    time.sleep(0.01)
    print(i)

print("holding for 5 seconds, then spinning down-then-up to -20% throttle...")
time.sleep(5)
      
print("spinning down")

while i > 1400:
    i = i-1;
    pi.set_servo_pulsewidth(ESC, i) # 0 throttle
    time.sleep(0.01)
    print(i)

print("holding for 5 seconds, then spinning down to 0% throttle...")
time.sleep(5)

while i < 1500:
    i = i+1;
    pi.set_servo_pulsewidth(ESC, i) # 0 throttle
    time.sleep(0.01)
    print(i)

print("should be 0% throttle, hold for 2 seconds, then shut off...")
time.sleep(2)

pi.set_servo_pulsewidth(ESC, 1500) # Stop servo pulses.

pi.stop() # Disconnect from local Raspberry Pi.
print("end")
