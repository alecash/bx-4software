import time
import RPi.GPIO as GPIO   # Import the GPIO library.

import pigpio

ESC = 13

pi = pigpio.pi() # Connect to local Pi.

# Calibrate ESC - *NOT NEEDED IF YOU HEAR LOW THEN HIGH BEEP WHEN CONNECTING TO RPI*
print("Calibrating the ESC, should hear low then high beep")
pi.set_servo_pulsewidth(SERVO, 1000) # Minimum throttle.
time.sleep(1)
pi.set_servo_pulsewidth(SERVO, 2000) # Maximum throttle.
time.sleep(1)

pi.set_servo_pulsewidth(ESC, 1500) # 0 throttle
time.sleep(3)

i = 1500;
while i < 1750:
    i = i+1;
    pi.set_servo_pulsewidth(ESC, i) # 0 throttle
    time.sleep(0.01)
    print(i)