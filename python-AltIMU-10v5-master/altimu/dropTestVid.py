import picamera
from time import sleep

with picamera.PiCamera() as camera: #have the camera instance running for the entire time we collect data
    camera.resolution = (640, 480)
    camera.start_recording('/home/pi/Documents/Altimu/bx-4software/python-AltIMU-10v5-master/altimu/testVid.h264')
    
    sleep(10)
    
    camera.stop_recording
        
