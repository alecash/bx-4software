#!/usr/bin/python

import os
import time
import picamera
from time import sleep     # Import the sleep function from the time module
import RPi.GPIO as GPIO         # Import Raspberry Pi GPIO library
from datetime import datetime
from constants import *         # Includes addresses on I2C bus
from lsm6ds33 import LSM6DS33   # Accel & Gyro (+ temp)
from lis3mdl import LIS3MDL     # Magnetometer (+ temp)
from lps25h import LPS25H       # Barometric Pressure & Temperature
import os.path                  # For File Handling
from multiprocessing import Process

totalDataCollectionTime = 120     #total time collectiong data after button press
lineCutterSignalTime = 60        #time after button press that a singal is sent to the line cutter
servoSignalTime = 90            #time after button press that a singal is sent to the servo


GPIO.setwarnings(False) # Ignore warning for now
GPIO.setmode(GPIO.BOARD) # Use physical pin numbering
GPIO.setup(10, GPIO.IN, pull_up_down=GPIO.PUD_DOWN) # Set pin 10 to be an input pin and set initial value to be pulled low (off)
GPIO.setup(11, GPIO.OUT, initial=GPIO.LOW)   # Set pin 11 to be an output pin and set initial value to low (off)
GPIO.setup(13, GPIO.OUT, initial=GPIO.LOW)   # Set pin 13 to be an output pin and set initial value to low (off)
GPIO.setup(15, GPIO.OUT, initial=GPIO.LOW)   # Set pin 15 to be an output pin and set initial value to low (off)

imu = LSM6DS33()        # Accelerometer and Gyroscope
imu.enable()

magnet = LIS3MDL()      # Magnetometer
magnet.enable()

baro = LPS25H()         # Barometric and Temperature
baro.enable()

###### Start File Name Handling ######
numFiles = 0      #make a variable to append to the end of the new file name
found = False     #boolean to help find how many files we have already

while not found:
    if not os.path.isfile('/home/pi/Documents/Altimu/bx-4software/python-AltIMU-10v5-master/altimu/data_log_%s.csv' % numFiles):
        found = True
    else:
        numFiles += 1
###### End File Name Handling ######
        
#create a new data log file
file = open('/home/pi/Documents/Altimu/bx-4software/python-AltIMU-10v5-master/altimu/data_log_%s.csv' % numFiles, 'w+')
file.write("Time,X-Gyro,Y-Gyro,Z-Gyro,X-Accel,Y-Accel,Z-Accel,X-Mag,Y-Mag,Z-Mag,Pressure,Altitude,Temperature\n")

def dataCollection():
#     GPIO.output(11, GPIO.HIGH) # Turn on
    
    #start data collection loop for totalDataCollectionTime seconds
    while time.time() - start_time < totalDataCollectionTime:
        
        #send a 1 sec signal to line cutter after LineCutterSignalTime seconds
        if time.time() - start_time > lineCutterSignalTime and time.time() - start_time < lineCutterSignalTime + 1:
            GPIO.output(13, GPIO.HIGH)
        else:
            GPIO.output(13, GPIO.LOW)
            
        #send a 1 sec signal to servo after servoSignalTime seconds
        if time.time() - start_time > servoSignalTime and time.time() - start_time < servoSignalTime + 1:
            GPIO.output(15, GPIO.HIGH)
        else:
            GPIO.output(15, GPIO.LOW)
            
        #make new strings to edit
        gyroStr = str(imu.getGyroscopeDPS())
        accStr = str(imu.getAccelerometerMPS2())
        magStr = str(magnet.getMagnetometerRaw())
            
        #chop brackets off the end of strings
        gyroStr = gyroStr[1:len(gyroStr) - 1]
        accStr = accStr[1:len(accStr) - 1]
        magStr = magStr[1:len(magStr) - 1]
        
        #get rid of whitespace
        gyroStr = gyroStr.replace(" ", "")
        accStr = accStr.replace(" ", "")
        magStr = magStr.replace(" ", "")

        #write imu data to file
        now = datetime.now()
        #file.write(str(now)+","+str(imu.getGyroscopeDPS())+","+str(imu.getAccelerometerMPS2())+","+str(magnet.getMagnetometerRaw())+","+str(baro.getBarometerMillibars())+","+str(baro.getAltitude())+","+str(baro.getTemperatureCelsius())+"\n")
        file.write(str(now)+","+gyroStr+","+accStr+","+magStr+","+str(baro.getBarometerMillibars())+","+str(baro.getAltitude())+","+str(baro.getTemperatureCelsius())+"\n")
        
    
def videoRecord():
    #create a camera instance and record for totalDataCollectionTime seconds
    with picamera.PiCamera() as camera:
        camera.resolution = (640, 480)
        camera.start_recording('/home/pi/Documents/Altimu/bx-4software/python-AltIMU-10v5-master/altimu/testVid_%s.h264' % numFiles)
        camera.wait_recording(totalDataCollectionTime)
        camera.stop_recording()
        

running = True;
while running:
    
    GPIO.output(11, GPIO.HIGH) # Turn on
    sleep(0.5)                  # Sleep for .5 second
    GPIO.output(11, GPIO.LOW)  # Turn off
    sleep(0.5)                  # Sleep for .5 second
    
    if GPIO.input(10) == GPIO.HIGH: #if button is pressed
        GPIO.output(11, GPIO.HIGH) # Turn on

        start_time = time.time()
        
        p1 = Process(target = dataCollection)
        p1.start()
        p2 = Process(target = videoRecord)
        p2.start()
      
        p1.join()
        p2.join()
        
        running = False;
        
        file.close()
        GPIO.output(11, GPIO.LOW)  # Turn off
        GPIO.output(13, GPIO.LOW)  # Turn off
        GPIO.output(15, GPIO.LOW)  # Turn off
    #end if statement for button press
        
#print("End script")