import cv2 as cv

def rescaleFrame(frame,scale=0.75):
    width = int(frame.shape[1]*scale)
    height = int(frame.shape[0]*scale)
    dimensions = (width,height)
    return cv.resize(frame,dimensions,interpolation=cv.INTER_AREA)

img = cv.imread(r"C:\Users\tdatt\OneDrive\Documents\Computer Vision\rocket.png")
img_resized = rescaleFrame(img,0.5)
cv.imshow('resized image', img_resized)

#img_blur = cv.GaussianBlur(img_resized, (5,5), cv.BORDER_DEFAULT)

# take out canny edges of certain size or smaller
img_blur = cv.blur(img_resized, (6,7))

cv.imshow('Blur',img_blur)

img_canny = cv.Canny(img_resized,150,150)
cv.imshow('Canny Edges', img_canny)
img_canny_blur = cv.Canny(img_blur,150,150)
cv.imshow('Canny Edges of blur', img_canny_blur)

contours, hierarchies = cv.findContours(img_canny_blur,cv.RETR_LIST,cv.CHAIN_APPROX_SIMPLE)
print(f'{len(contours)} contour(s) found!')

cv.waitKey(0)