#!/usr/bin/python3

import RPi.GPIO as GPIO
import time
import threading
import altimu
import multiprocessing

def PID():
    print("Control")

def Cam():
    print("Camera")

def Traj():
    print("Trajectory")

def Sensor():
    print("Sensor")
    # Uncomment when IMU is connected and to be used
#     A = AltIMU()
#     A.enable()  #enables all functions of the IMU
#     
#     while true:
#         accel_file = open("Acceleration_Data.txt","a")
#         accel = A.getAcceleration()
#         accel_file.write(accel)
#         accel_file.close()
#         gyro_file = open("Gyroscope_Data.txt","a")
#         gyro = A.getGyroscopeDPS()
#         gyro_file.write(gyro)
#         gyro_file.close()
#         mag_file = open("Magnetometer_Data.txt","a")
#         mag = A.getMagnetometerRaw()
#         mag_file.write(mag)
#         mag_file.close()
#         alt_file = open("Altitude_Data.txt","a")
#         alt = A.getAltitude()
#         alt_file.write(alt)
#         alt_file.close()

#TODO: configure GPIOs
GPIO.setmode(GPIO.BCM)
# Rocket Signal
GPIO.setup(23, GPIO.IN)
# Mosfet
GPIO.setup(17, GPIO.OUT)
# Parachute deployment
GPIO.setup(27, GPIO.OUT)

# wait for launch signal, honestly might not even need this
#GPIO.wait_for_edge(10, GPIO.RISING)

# do we need to start spinning up the RWA on ascent? nope

# could either be falling or rising on a different pin
# wait for deployment signal
# if not signal after 150s, deploy anyway
#GPIO.wait_for_edge(10, GPIO.FALLING, timeout=150000)
print("past set up")
# TODO: flip mosfet
GPIO.output(17, 1)

#Initialize Sensor Data
accel = 0
gyro = 0
mag = 0
alt = 0
print("data initialized")

try:
    sensor_thread = multiprocessing.Process(target=Sensor)
    sensor_thread.start()
except:
    print("Error: unable to start sensor thread")
try:
    control_thread = multiprocessing.Process(target=PID)
    control_thread.start()
except:
    print("Error: unable to start control thread")

try:
    camera_thread = multiprocessing.Process(target=Cam)
    camera_thread.start()
except:
    print("Error: unable to start camera thread")
    
# this might need to start at launch
try:
    traj_thread = multiprocessing.Process(target=Traj)
    traj_thread.start()
except:
    print("Error: unable to start trajectory thread")

while alt > 105000:
    time.sleep(0.2)


#Deploy Parachute
GPIO.output(27, 1)

# Join all threads
control_thread.join()
camera_thread.join()
traj_thread.join()
sensor_thread.join()

GPIO.cleanup()
