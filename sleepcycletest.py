#!/usr/bin/python3

import RPi.GPIO as GPIO
import time

GPIO.setmode(GPIO.BCM)
GPIO.setup(4, GPIO.IN)

print("Haven't died yet")

#launch = False

#print(launch)

# GPIO.wait_for_edge(4, GPIO.RISING)

#reading in as true even though it isn't connected to anything, we'll deal with that later

launch = GPIO.input(4)

waiting = 0

while launch is 1:
    time.sleep(20)
    waiting += 20
    print(waiting)
    launch = GPIO.input(4)


print(launch)